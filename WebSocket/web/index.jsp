<%--
  Created by IntelliJ IDEA.
  User: 2
  Date: 2020/4/28
  Time: 18:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>aa</title>
  </head>
  <body>

  <div style="width:400px;margin:20px auto;border:1px solid #30c8d3;padding:20px;text-align:center;">
    当前学生名单<span style="color:#FF7519" id="price">杨淮尊</span>
    <div style="font-size:0.9em;margin-top:20px">学生在线人数 <span id="total">1</span> 个人在线</div>
  </div>

  <div style="text-align: center;background-color: rgba(129,86,255,0.35);margin:0 auto;border:1px solid #000;width:600px;height:650px">
    <textarea id="content" cols="60" rows="30" readonly="readonly"></textarea><br>
    <input type="text" id="sendMsg">
    <button type="button" onclick="sendMsg()">发送消息</button>
  </div>

  </body>
  <%-- 学员实时名单 --%>
  <script type="text/javascript">
    var websocket = null;
    //判断当前浏览器是否支持WebSocket
    if ('WebSocket' in window) {
      websocket = new WebSocket("ws://39.107.120.200:8080/bitcoin/ws/countStu");

      //连接成功建立的回调方法
      websocket.onopen = function () {
        console.log("客户端链接成功");
      }

      //接收到消息的回调方法
      websocket.onmessage = function (event) {
        setMessageInnerHTML(event.data);
      }

      //连接发生错误的回调方法
      websocket.onerror = function () {
        console.log("WebSocket连接发生错误");
      };

      //连接关闭的回调方法
      websocket.onclose = function () {
        console.log("WebSocket连接关闭");
      }

      //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
      window.onbeforeunload = function () {
        closeWebSocket();
      }
    }
    else {
      console.log('当前浏览器 Not support websocket');
    }
    //将消息显示在网页上
    function setMessageInnerHTML(innerHTML) {
      var bitcoin = eval("("+innerHTML+")");
      document.getElementById('price').innerHTML = bitcoin.name;
      document.getElementById('total').innerHTML = bitcoin.total;
    }
    //关闭WebSocket连接
    function closeWebSocket() {
      websocket.close();
      websocket = null;
    }

  </script>
<%-- 实时聊天 --%>
  <script type="text/javascript">
    var ws = null;
    var url = "ws://39.107.120.200:8080/bitcoin/page_room/";
    if ('WebSocket' in window) {
        //获取用户名
        var username = "yhz";
        ws = new WebSocket(url + username);
        //与服务端建立连接触发
        ws.onopen = function () {
          console.log("与服务器成功建立连接");
        };
        //服务端推送消息触发
        ws.onmessage = function (event) {
          talking(event.data);
        };
        //发生错误触发
        ws.onerror = function () {
          console.log("连接错误")
        };
        //正常关闭触发
        ws.onclose = function () {
          console.log("连接关闭");
        };
    }
    else {
      console.log('Not support websocket');
    }

    function sendMsg() {
      if(!ws){
        alert("你已掉线，请重新加入");
        return;
      }
      //消息发送
      ws.send(document.getElementById("sendMsg").value);
      document.getElementById("sendMsg").value = "";
    }
    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function () {
      closeWebSocket();
    }

    function closeWebSocket() {
      if(ws){
        ws.close();
        ws = null;
      }
    }
    function talking(content) {
      document.getElementById("content").append(content + "\r\n");
    }
  </script>
</html>