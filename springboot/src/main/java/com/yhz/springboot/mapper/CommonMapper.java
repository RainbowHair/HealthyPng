package com.yhz.springboot.mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:24 2020/1/21
 */
public interface CommonMapper {
    //查询会员信息
    public List<Map> select(Map map);
    //添加健康信息
    public void  addInfo(Map map);
}
