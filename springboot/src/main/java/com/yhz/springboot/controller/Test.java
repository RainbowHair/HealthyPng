package com.yhz.springboot.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author yhz
 * @Data 8:03 2020/2/29
 * @Description
 */
public class Test {
    public static void main(String[] args) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String format = simpleDateFormat.format(date);
        System.out.println("Date:"+format);
    }
}
