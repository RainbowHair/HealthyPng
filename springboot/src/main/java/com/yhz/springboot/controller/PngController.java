package com.yhz.springboot.controller;

import com.alibaba.fastjson.JSON;
import com.yhz.springboot.service.CommonService;
import com.yhz.springboot.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @Author yhz
 * @Data 7:27 2020/2/29
 * @Description
 */
@RestController
@RequestMapping("png")
public class PngController {
    
    @Autowired
    CommonService service;
    
    @RequestMapping(value = "/",method = RequestMethod.POST)
    public void png(@RequestParam("file_info")MultipartFile file,@RequestParam("type") String maps) throws IOException {
        Map map = JSON.parseObject(maps, Map.class);
        //获取时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String format = simpleDateFormat.format(date);
        map.put("NowDate",format);
        //存入数据库
        service.addInfo(map);
        //本地文件路径
        String LocalPath="F:\\FileTest\\ToLocal";
        //调用工具类
        FileUtil.upload(file,LocalPath);
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String png2() throws IOException {
        return "测试";
    }
}
