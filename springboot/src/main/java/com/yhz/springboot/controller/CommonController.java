package com.yhz.springboot.controller;

import com.alibaba.fastjson.JSON;
import com.yhz.springboot.service.CommonService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:32 2020/1/21
 */
@RestController
@RequestMapping("/member")
public class CommonController {
    @Resource
    CommonService service;
    
    @RequestMapping(value = "/{page}",method = RequestMethod.GET)
    public String select(@PathVariable Map map){
        System.out.println(map);
        List<Map> select = service.select(map);
        System.out.println(select);
        return JSON.toJSONString(select);
    }
    
}
