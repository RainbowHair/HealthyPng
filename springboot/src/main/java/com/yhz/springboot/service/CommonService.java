package com.yhz.springboot.service;

import com.yhz.springboot.mapper.CommonMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:26 2020/1/21
 */
@Service
public class CommonService implements CommonMapper {

    @Autowired
    SqlSessionTemplate sqlSession;

    //查询会员信息
    @Override
    public List<Map> select(Map map) { return sqlSession.selectList("com.yhz.springboot.mapper.CommonMapper.select",map);}
    //新增健康信息
    @Override
    public void addInfo(Map map) {
        sqlSession.selectList("com.yhz.springboot.mapper.CommonMapper.addInfo",map);
    }
}
